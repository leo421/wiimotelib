﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WiimoteLib;
using System.IO.Pipes;

namespace WiimoteTest
{
	public partial class MultipleWiimoteForm : Form
	{
        const string PIPE_NAME = "WiiBB";
		// map a wiimote to a specific state user control dealie
		Dictionary<Guid,WiimoteInfo> mWiimoteMap = new Dictionary<Guid,WiimoteInfo>();
		WiimoteCollection mWC;
        //NamedPipeServerStream mPipeServer = new NamedPipeServerStream("WiiBB");
        NamedPipeClientStream mPipeClient = new NamedPipeClientStream(".", PIPE_NAME, PipeDirection.Out);


		public MultipleWiimoteForm()
		{
			InitializeComponent();
		}

		private void MultipleWiimoteForm_Load(object sender, EventArgs e)
		{
			// find all wiimotes connected to the system
			mWC = new WiimoteCollection();
			int index = 1;

            mPipeClient.Connect();
            
			try
			{
				mWC.FindAllWiimotes();
			}
			catch(WiimoteNotFoundException ex)
			{
				MessageBox.Show(ex.Message, "Wiimote not found error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			catch(WiimoteException ex)
			{
				MessageBox.Show(ex.Message, "Wiimote error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Unknown error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

			foreach(Wiimote wm in mWC)
			{
				// create a new tab
				TabPage tp = new TabPage("Wiimote " + index);
				tabWiimotes.TabPages.Add(tp);

				// create a new user control
				WiimoteInfo wi = new WiimoteInfo(wm);
				tp.Controls.Add(wi);

				// setup the map from this wiimote's ID to that control
				mWiimoteMap[wm.ID] = wi;

				// connect it and set it up as always
				wm.WiimoteChanged += wm_WiimoteChanged;
				wm.WiimoteExtensionChanged += wm_WiimoteExtensionChanged;

				wm.Connect();
				if(wm.WiimoteState.ExtensionType != ExtensionType.BalanceBoard)
					wm.SetReportType(InputReport.IRExtensionAccel, IRSensitivity.Maximum, true);
				
				wm.SetLEDs(index++);
			}
		}

		void wm_WiimoteChanged(object sender, WiimoteChangedEventArgs e)
		{
			WiimoteInfo wi = mWiimoteMap[((Wiimote)sender).ID];
            /*
            //Console.WriteLine(e.WiimoteState.BalanceBoardState.SensorValuesKg.TopRight);
            Console.WriteLine(string.Format("{0:F6},    {1:F6},    {2:F6},    {3:F6},    {4:F6},    {5:F6},    {6:F6},    {7:F6}",
                e.WiimoteState.BalanceBoardState.SensorValuesKg.TopLeft,
                e.WiimoteState.BalanceBoardState.SensorValuesKg.TopRight,
                e.WiimoteState.BalanceBoardState.SensorValuesKg.BottomLeft,
                e.WiimoteState.BalanceBoardState.SensorValuesKg.BottomRight,
                (e.WiimoteState.BalanceBoardState.SensorValuesKg.TopLeft + e.WiimoteState.BalanceBoardState.SensorValuesKg.TopRight + e.WiimoteState.BalanceBoardState.SensorValuesKg.BottomLeft + e.WiimoteState.BalanceBoardState.SensorValuesKg.BottomRight)/4,
                e.WiimoteState.BalanceBoardState.WeightKg,
                e.WiimoteState.BalanceBoardState.CenterOfGravity.X,
                e.WiimoteState.BalanceBoardState.CenterOfGravity.Y
                ));
            byte[] byteArray = BitConverter.GetBytes(e.WiimoteState.BalanceBoardState.WeightKg);
            foreach (byte b in byteArray)
            {
                Console.Write(b.ToString("X2") + " ");
            }
            Console.WriteLine("");
            mPipeClient.Write(byteArray,0,byteArray.Length);
            */
            sendData(e.WiimoteState.BalanceBoardState);
			wi.UpdateState(e);
		}

        void sendData(BalanceBoardState bbs)
        {
            byte[] fd;
            byte[] outdata = new byte[24];
            float[] data_array = {
                                     bbs.SensorValuesKg.TopLeft,
                                     bbs.SensorValuesKg.TopRight,
                                     bbs.SensorValuesKg.BottomLeft,
                                     bbs.SensorValuesKg.BottomRight,
                                     bbs.CenterOfGravity.X,
                                     bbs.CenterOfGravity.Y
                                 };
            int i = 0;
            foreach (float d in data_array)
            {
                fd = BitConverter.GetBytes(d);
                Array.Copy(fd, 0, outdata, i, 4);
                i += 4;
            }
            mPipeClient.Write(outdata, 0, 24);
            
        }

		void wm_WiimoteExtensionChanged(object sender, WiimoteExtensionChangedEventArgs e)
		{
			// find the control for this Wiimote
			WiimoteInfo wi = mWiimoteMap[((Wiimote)sender).ID];
			wi.UpdateExtension(e);

			if(e.Inserted)
				((Wiimote)sender).SetReportType(InputReport.IRExtensionAccel, true);
			else
				((Wiimote)sender).SetReportType(InputReport.IRAccel, true);
		}

		private void MultipleWiimoteForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			foreach(Wiimote wm in mWC)
				wm.Disconnect();
		}
	}
}
